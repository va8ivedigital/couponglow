@extends('web.layouts.app')
@section('content')
@php
$imgHolder = 'data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
@endphp
<style>
.banner .slider-for .slide {
    max-height: unset;
}
</style>
<div class="banner">

    <div class="slider-for">

    @if(!empty($banners))
        @foreach($banners as $banner)
        <a href="{{ $banner['link'] }}" target="_blank" title="{{ isset($banner['title']) ? $banner['title'] : '' }}">
            <div class="slide">

                <picture>

                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($banner['image']['url']) ? $banner['image']['url'] : '' }}" alt="{{ isset($banner['title']) ? $banner['title'] : '' }}">

                </picture>

            </div>
        </a>
        @endforeach
    @endif

    </div>

</div>
<div class="innerContainer">
@if(!isset($banners))
            <picture>
                <source
                    media="(min-width: 650px)"
                    srcset="{{config('app.image_path')}}/build/images/404.jpg">
            <!-- <source
        media="(max-width: 649px)"
        srcset="{{config('app.image_path')}}/build/images/banner1_res.jpg"> -->
                <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{config('app.image_path')}}/build/images/404.jpg" alt="404">
            </picture>            
        @endif
</div>
<div class="innerContainer">

    <div class="topStores">

        <h2 class="pageHeading">{{ trans('sentence.home_top_stores') }}</h2>

        <div class="slidesToShow5">

            @if(!empty($popularStores))
                @foreach($popularStores as $store)
                <div class="slide">

                    <a href="{{config('app.app_path')}}/{{ isset($store['slugs']['slug']) ? $store['slugs']['slug'] : '' }}" class="image">

                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($store['image']['url']) ? $store['image']['url'] : '' }}" alt="">

                    </a>

                </div>
                @endforeach
            @endif

        </div>

    </div>

</div>
<div class="innerContainer">

    <div class="featureDeals">

        <h3 class="pageHeading">{{ trans('sentence.home_featured_deals_and_about') }}</h3>

        <div class="rowbar">

            <div class="flexWrap">

                @if(!empty($featuredCouponsAndPopularCoupons))
                @foreach($featuredCouponsAndPopularCoupons as $featuredCoupons)
                @if($featuredCoupons['featured'] == 1)

                <div class="productBox">

                    <div class="coupons">

                        <figure>

                            <a href="javascript:;" class="logoAnchor">

                            <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($featuredCoupons['image']['url']) ? $featuredCoupons['image']['url'] : '' }}" alt="{{ $featuredCoupons['title'] }}" class="couponImage">

                            </a>

                        </figure>

                        <div class="cpnDtlSec">

                            <div class="textWrpr">

                                <div class="title">

                                    <h4 class="offerTitle">{{ isset($featuredCoupons['title']) ? $featuredCoupons['title'] : '' }}</h4>

                                    <p class="offerDesr">{{ isset($featuredCoupons['title']) ? $featuredCoupons['title'] : '' }}</p>

                                </div>

                                <div class="expiry">

                                    <div class="views">

                                        <i class="lm_user"></i><span>{{$featuredCoupons['viewed']}} {{ trans('sentence.home_coupon_views') }}</span>

                                    </div>

                                    @if($featuredCoupons['verified']==1)
                                    <div class="verify">

                                        <i class="lm_check_square"></i><span>{{ trans('sentence.home_verified') }}</span>

                                    </div>
                                    @endif

                                    <div class="date">

                                        <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ date('d-M-yy', strtotime($featuredCoupons['date_expiry'])) }}</span>

                                    </div>

                                </div>

                            </div>

                            @if(!empty($featuredCoupons['code']))
                            <div class="buttonsWrapper">

                                <div class="codeButton openOverlay" data-name="copycode">

                                    <a class="baseurlappend visibleButton" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" data-marchant="{{ $featuredCoupons['affiliate_url'] }}" data-var="copy" title="">

                                    <span class="buttonSpan"> {{ trans('sentence.home_get_code') }} </span>

                                    </a>

                                    <span class="hiddenCode"> <?php echo uniqid(); ?> <span class="foldedCorner"></span> </span>

                                </div>

                            </div>
                            <i class="lm_right vsbanchr"></i>
                            <a class="cids responsiveLink" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" title=""></a>
                            @else

                            <div class="buttonsWrapper">
                                <div class="codeButton">
                                    <a class="DealButton sids" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" data-marchant="{{ $featuredCoupons['affiliate_url'] }}" data-var="deal">{{ trans('sentence.home_get_deal') }}
                                    </a>
                                </div>
                            </div>

                            @endif

                        </div>

                    </div>

                </div>
                @endif
                @endforeach
                @endif

            </div>

        </div>

    </div>

</div>
<div class="innerContainer">

    <div class="recommendedDeals">

        <h3 class="pageHeading">{{ trans('sentence.home_top_recommended') }}</h3>

        <h4 class="subHeading">{{ trans('sentence.home_all_coupons_and_deals') }}</h4>

        <div class="rowbar">

            <div class="flexWrap">

                <!-- add class fullPrd on productBox to implement full style coupon -->
                @if(!empty($featuredCouponsAndPopularCoupons))
                @foreach($featuredCouponsAndPopularCoupons as $featuredCoupons)
                @if($featuredCoupons['popular'] == 1)

                    <div class="productBox fullPrd">

                        <div class="coupons">

                            <figure>

                                <a href="javascript:;" class="logoAnchor">

                                @if(!empty($featuredCoupons['image']['url']))
                                <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($featuredCoupons['image']['url']) ? $featuredCoupons['image']['url'] : '' }}" alt="" class="couponImage">
                                @else
                                <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{config('app.image_path')}}/build/images/blog/imagePlaceHolder336.png" alt="" class="couponImage">
                                @endif

                                </a>

                            </figure>

                            <div class="cpnDtlSec">

                                <div class="textWrpr">

                                    <div class="title">

                                        <h4 class="offerTitle">{{ isset($featuredCoupons['title']) ? $featuredCoupons['title'] : '' }}</h4>

                                        <span>{{ trans('sentence.home_exclusive') }}</span>

                                    </div>

                                    <div class="expiry">

                                        <div class="views">

                                            <i class="lm_user"></i><span>{{ $featuredCoupons['viewed'] }} {{ trans('sentence.home_coupon_views') }}</span>

                                        </div>

                                        @if($featuredCoupons['verified']==1)
                                        <div class="verify">

                                            <i class="lm_check_square"></i><span>{{ trans('sentence.home_verified') }}</span>

                                        </div>
                                        @endif

                                        <div class="date">
                                            <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ date('d-M-yy', strtotime($featuredCoupons['date_expiry'])) }}</span>
                                        </div>

                                    </div>

                                </div>
                                @if(!empty($featuredCoupons['code']))
                                <div class="buttonsWrapper">

                                    <div class="codeButton openOverlay" data-name="copycode">

                                        <a class="baseurlappend visibleButton" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" data-marchant="{{ $featuredCoupons['affiliate_url'] }}" data-var="copy" title="">

                                        <span class="buttonSpan"> {{ trans('sentence.home_get_code') }} </span>

                                        </a>

                                        <span class="hiddenCode"> <?php echo uniqid(); ?> <span class="foldedCorner"></span> </span>

                                    </div>

                                </div>
                            <i class="lm_right vsbanchr"></i>
                            <a class="cids responsiveLink" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" title=""></a>
                            @else

                            <div class="buttonsWrapper">
                                <div class="codeButton">
                                    <a class="DealButton sids" data-id="{{ $featuredCoupons['id'] }}" data-store="{{ $featuredCoupons['affiliate_url'] }}" data-marchant="{{ $featuredCoupons['affiliate_url'] }}" data-var="deal">{{ trans('sentence.home_get_deal') }}
                                    </a>
                                </div>
                            </div>

                            @endif

                            </div>


                        </div>

                    </div>
                @endif
                @endforeach
                @endif

            </div>

        </div>

    </div>

</div>
<div class="innerContainer">
    <div class="webSubDomain">
        <div class="rowbar">
            <div class="Flx">
                @if(!empty($sites))
                    @foreach($sites as $site)
                        <div class="subDom">
                            <a href="{{ isset($site['country_code']) ? url(strtolower($site['country_code'])) : '' }}">
                                <picture>
                                    @if(!empty($site['flag']['url']))
                                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{ isset($site['flag']['url']) ? $site['flag']['url'] : '' }}" class="flags" alt="" />
                                    @else
                                    <img src="{{config('app.image_path')}}/build/images/blog/imagePlaceHolder336.png" alt="">
                                    @endif
                                </picture>
                                <span>{{ isset($site['country_name']) ? $site['country_name'] : '' }}</span>
                            </a>
                        </div>
                    @endforeach
                @endif


            </div>
        </div>
    </div>
</div>




@endsection