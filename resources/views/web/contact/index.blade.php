@extends('web.layouts.app')
@section('content')
<div class="breadcrumb">
    <ul>
        <li>
            <a href="{{ config('app.app_path') }}"><i class="lm_home"></i> Home</a>
        </li>
        <li>
            <a href="{{ config('app.app_path') }}/contact_us">{{ trans('sentence.contact_page_name') }}</a>
        </li>
    </ul>
</div>
<div class="flexWrapper contactWrp">

    <div class="contntWrpr">

        <h1 class="contactHeading"><span>{{ trans('sentence.contact_page_contact_heading') }}</span></h1>

        <div class="map">

            <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ config('app.image_path') }}/build/images/contactccmap.jpg" alt="">

        </div>

        <div class="formWrapper">

            <h2>{{ trans('sentence.contact_your_data_will_be_safe') }}</h2>

            <p>{{ trans('sentence.contact_email_not_be_publish') }}</p>

            <div class="rowbar outerContactBox">

                <form id="contactBox" action="{{ route("contact.store") }}" enctype="multipart/form-data">

                    <div class="inputWrapper fullColumn">

                        <label>{{ trans('sentence.contact_your_name') }}</label>

                        <input type="text" name="name" value=""  id="name" required="">

                    </div>

                    <div class="inputWrapper fullColumn">

                        <label>{{ trans('sentence.contact_email_address') }}</label>

                        <input type="email" name="email" value="" id="email" required="">

                    </div>

                    <div class="inputWrapper fullColumn">

                        <label>{{ trans('sentence.contact_subject') }}</label>

                        <input type="text" name="subject" id="subject" value="" required="">

                    </div>

                    <div class="inputWrapper fullColumn">

                        <label>{{ trans('sentence.contact_message') }}</label>

                        <textarea name="message" rows="10"  id="message"></textarea>

                    </div>

                    <div class="fullColumn">

                        <button type="submit">{{ trans('sentence.contact_submit_button') }}</button>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="sidCntnt">

        <h1 class="contactHeading"><span>{{ trans('sentence.contact_page_contact_heading') }}</span></h1>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.contact_related_categories') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($featuredCategories))
                    @foreach($featuredCategories as $featuredCategory)
                        <li><a href="{{ config('app.app_path') }}/{{ $featuredCategory['slugs']['slug'] }}">{{ $featuredCategory['title'] }}</a></li>
                    @endforeach
                @endif

            </ul>

            <div class="buttons">

                <a href="{{config('app.app_path')}}/category" class="relCatBtn">{{ trans('sentence.contact_view_all') }} <span>{{ trans('sentence.contact_view_all_categories') }}</span></a>

            </div>

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.contact_popular_brands') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($popularStores))
                    @foreach($popularStores as $popularStore)
                        <li><a href="{{ $popularStore['slugs']['slug'] }}">{{ $popularStore['name'] }}</a></li>
                    @endforeach
                @endif

            </ul>

        </div>

        <div class="sidePannel cpnAlert subscribeNeverMisNewLetter">

            <h4>{{ trans('sentence.contact_coupon_alerts') }}</h4>

            <p>{{ trans('sentence.contact_never_miss_best_coupon') }}</p>
            <form id="subscribeNeverMisId">
                <input type="email" required="required" id="subscribeNeverMis" placeholder="{{ trans('sentence.contact_place_holder') }}">
            </form>
        </div>

    </div>

</div>

@endsection