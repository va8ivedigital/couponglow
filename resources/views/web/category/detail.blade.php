@extends('web.layouts.app')
@section('content')
<div class="breadcrumb">
    <ul>
        <li>
            <a href="{{config('app.app_path')}}"><i class="lm_home"></i> Home</a>
        </li>
        <li>
            <a href="{{config('app.app_path')}}/category">{{ trans('sentence.category_detail_categories') }}</a>
        </li>
        <li>
            <a href="{{config('app.app_path')}}/{{$detail['slug']}}">{{$detail['title']}}</a>
        </li>
    </ul>
</div>
<div class="flexWrapper fullpage">

    <div class="contntWrpr">

        <h2 class="pageHeading">{{ $detail['title'] }} {{ trans('sentence.category_detail_popular_stores') }}</h2>

        <div class="rowbar">

            <div class="popularStore brandLogo">

                <div class="flexWrap">

                    @if(!empty($detail->categoryStores))
                        @foreach($detail->categoryStores as $store)
                        <div class="stores">

                            <a href="{{config('app.app_path').'/'.(isset($store['slugs']['slug']) ? $store['slugs']['slug'] : '')}}" class="image">

                            <span>
                            @if(!empty($store['image']['url']))
                            <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($store['image']['url']) ? $store['image']['url'] : '' }}" alt="">
                            @else
                            <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{config('app.image_path')}}/build/images/blog/imagePlaceHolder336.png" alt="">
                            @endif

                            </span>

                            </a>

                        </div>
                        @endforeach
                    @endif

                    <div class="buttons">

                        <a href="{{ config('app.app_path') }}/sitemap" class="btn viewStoreBtn">{{ trans('sentence.category_detail_view_all') }}</a>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection