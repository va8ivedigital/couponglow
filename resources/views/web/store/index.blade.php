@extends('web.layouts.app')
@section('content')
<div class="breadcrumb">
    <ul>
        <li>
            <a href="{{config('app.app_path')}}/"><i class="lm_home"></i> Home</a>
        </li>
        <li>
            <a href="{{config('app.app_path')}}/sitemap">{{ trans('sentence.sitemap_page_name') }}</a>
        </li>
    </ul>
</div>
<div class="flexWrapper">

    <div class="contntWrpr">

        <div class="allStore">

            <h2 class="pageHeading style1">{{ trans('sentence.sitemap_alphabetical_store_list') }}</h2>

            <div class="alphabtLsting">

                <ul>

                    <li @if(isset($_GET["q"]) && $_GET["q"] == '0-9') class="active" @endif>

                        <a class="sortalpha" data-target2="/sitemap?q=0-9" >{{ trans('sentence.sitemap_numeric_key') }}</a>

                    </li>
                    <?php $count = 0; ?>
                    @for($loop='a'; $loop <= 'z'; $loop++)
                    @if($count == 26)<?php break;?>@else<?php $count++;?>@endif
                        <li @if(isset($_GET["q"]) && $_GET["q"] == $loop) class="active" @endif><a class="sortalpha" data-target2="/sitemap?q={{ $loop }}">{{ trans('sentence.sitemap_search_by_'.$loop) }}</a></li>
                    @endfor

                </ul>

            </div>


            <div class="resultStore">
                {{ csrf_field() }}
                <div id="post_data" class="flexWrap">

                    @if(!empty($list))
                        @foreach($list as $store)
                        <div class="stores">

                            <a href="{{ config('app.app_path') }}/{{ $store['slugs']['slug'] }}" class="image">

                            <span>
                            @if(!empty($store['image']['url']))
                                <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ $store['image']['url'] }}" alt="">
                            @else
                                <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ config('app.image_path') }}/build/images/blog/imagePlaceHolder336.png" alt="">
                            @endif

                            </span>

                            </a>

                        </div>
                        @php
                            $last_id = $store['id'];
                        @endphp
                        @endforeach
                    @else
                        @php
                            $last_id = 0;
                        @endphp
                    @endif

                    <div id="blog_load_more_button"></div>

                    <div class="remove_last_id">
                        <input type="hidden" id="last_id" value="{{ $last_id }}">
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="sidCntnt">

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.sitemap_related_categories') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($featuredCategories))
                    @foreach($featuredCategories as $featuredCategory)
                        <li><a href="{{ config('app.app_path') }}/{{ $featuredCategory['slugs']['slug'] }}">{{ $featuredCategory['title'] }}</a></li>
                    @endforeach
                @endif

            </ul>

            <div class="buttons">

                <a href="{{config('app.app_path')}}/category" class="relCatBtn">{{ trans('sentence.sitemap_view_all') }}<span> {{ trans('sentence.sitemap_categories') }}</span></a>

            </div>

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.sitemap_popular_brands') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($popularStores))
                    @foreach($popularStores as $popularStore)
                        <li><a href="{{ $popularStore['slugs']['slug'] }}">{{ $popularStore['name'] }}</a></li>
                    @endforeach
                @endif

            </ul>

        </div>


        <div class="sidePannel cpnAlert subscribeNeverMisNewLetter">

            <h4>{{ trans('sentence.contact_coupon_alerts') }}</h4>

            <p>{{ trans('sentence.contact_never_miss_best_coupon') }}</p>
            <form id="subscribeNeverMisId">
                <input type="email" required="required" id="subscribeNeverMis" placeholder="{{ trans('sentence.contact_place_holder') }}">
            </form>
        </div>

    </div>

</div>
@endsection