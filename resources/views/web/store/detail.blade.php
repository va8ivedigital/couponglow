@extends('web.layouts.app')
@section('content')

<div class="flexWrapper strCpnDeal">

    <div class="contntWrpr">

        <div class="cupnsTextAlgment">

            <h3 class="pageTitle">{{$detail['name']}}</h3>

            <div class="rowbar">

                <div class="flexWrap cupnsAdjust">

                    @foreach($detail['store_coupons'] as $coupon)
                        @if($coupon['code']!='')
                        <div class="only-codes productBox fullPrd">

                            <div class="coupons">
                                @if(!empty($coupon['custom_image_title']))
                                <div class="custom_title_off logoAnchor" style="font-size: 16px;margin: 0;color: #000;line-height: 28px;font-weight: bold;text-align: center;">
                                    <h4>{{ $coupon['custom_image_title'] }}</h4>
                                </div>
                                @else
                                <figure>

                                    <a href="javascript:;" class="logoAnchor">

                                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($detail['image']['url']) ? $detail['image']['url'] : '' }}" alt="" class="couponImage">

                                    </a>

                                </figure>
                                @endif

                                <div class="cpnDtlSec">

                                    <div class="textWrpr">

                                        <div class="title">

                                            <h4 class="offerTitle">{{ isset($coupon['title']) ? $coupon['title'] : '' }}</h4>

                                            @if($coupon['exclusive']!=0)
                                                <span>{{ trans('sentence.home_exclusive') }}</span>
                                            @else
                                                <span>&nbsp;&nbsp;</span>
                                            @endif

                                        </div>

                                        <div class="expiry">

                                            <div class="views">

                                                <i class="lm_user"></i><span>{{ $coupon['viewed'] }} {{ trans('sentence.category_store_detail_views') }}</span>

                                            </div>

                                            <div class="verify">
                                                @if($coupon['verified']==1)
                                                <i class="lm_check_square"></i><span>{{ trans('sentence.category_store_detail_verified') }}</span>
                                                @endif
                                            </div>

                                            <div class="date">
                                                @php
                                                    if (date("Y", strtotime($coupon['date_expiry'])) >= '2090') {
                                                        $expiry = 'On Going';
                                                    } else {
                                                        $expiry = date("M-j-Y", strtotime($coupon['date_expiry']));
                                                    }
                                                @endphp
                                                <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ $expiry }}</span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="buttonsWrapper">
                                        <div class="codeButton openOverlay" data-name="copycode">
                                            <a class="baseurlappend visibleButton" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}" data-marchant="{{ $coupon['affiliate_url'] }}" data-var="copy">
                                            <span class="buttonSpan"> {{ trans('sentence.category_store_detail_codes') }} </span>
                                            </a>
                                            <span class="hiddenCode"> <?php echo uniqid(); ?> <span class="foldedCorner"></span> </span>
                                        </div>
                                    </div>

                                    

                                </div>

                                <i class="lm_right vsbanchr"></i>
                            <a class="cids responsiveLink" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}"></a>

                            </div>

                        </div>
                        @else
                        <div class="only-codes productBox fullPrd">

                            <div class="coupons">
                                @if(!empty($coupon['custom_image_title']))
                                <div class="custom_title_off logoAnchor" style="font-size: 16px;margin: 0;color: #000;font-weight: bold;text-align: center;">
                                    <h4>{{ $coupon['custom_image_title'] }}</h4>
                                </div>
                                @else
                                <figure>

                                    <a href="javascript:;" class="logoAnchor">

                                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{ isset($detail['image']['url']) ? $detail['image']['url'] : '' }}" alt="" class="couponImage">

                                    </a>

                                </figure>
                                @endif

                                <div class="cpnDtlSec">

                                    <div class="textWrpr">

                                        <div class="title">

                                            <h4 class="offerTitle">{{ isset($coupon['title']) ? $coupon['title'] : '' }}</h4>

                                            @if($coupon['exclusive']!=0)
                                            <span>{{ trans('sentence.home_exclusive') }}</span>
                                            @else
                                                <span>&nbsp;&nbsp;</span>
                                            @endif

                                        </div>

                                        <div class="expiry">

                                            <div class="views">

                                                <i class="lm_user"></i><span>{{ $coupon['viewed'] }} {{ trans('sentence.category_store_detail_views') }}</span>

                                            </div>

                                            @if($coupon['verified']==1)
                                            <div class="verify">

                                                <i class="lm_check_square"></i><span>{{ trans('sentence.category_store_detail_verified') }}</span>

                                            </div>
                                            @endif

                                            <div class="date">
                                                @php
                                                $expiryDate = date('d-M-yy', strtotime($coupon['date_expiry']));
                                                @endphp
                                                <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ $expiryDate }}</span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="buttonsWrapper">

                                        <div class="codeButton openOverlay" data-name="copycode">

                                            <a class="baseurlappend DealButton" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}" data-marchant="{{ $coupon['affiliate_url'] }}" data-var="deal">
                                            <span class="buttonSpan">{{ trans('sentence.category_store_detail_deals') }}</span>
                                            </a>

                                        </div>

                                    </div>

                                </div>

                                <i class="lm_right vsbanchr"></i>
                                <a class="cids responsiveLink" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}"></a>

                            </div>

                        </div>
                        @endif
                    @endforeach
                    

                </div>

            </div>

        </div>

    </div>

    <div class="sidCntnt">

        <div class="sidePannel abBrCnt">

            <h2>{{ trans('sentence.category_store_detail_about') }} {{ isset($detail['name']) ? $detail['name'] : '' }}</h2>
            <p style="text-align: justify;">

            {!!html_entity_decode($detail['short_description'])!!}

            </p>

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.category_detail_related_categories') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($featuredCategories))
                    @foreach($featuredCategories as $category)
                        <li><a href="{{ config('app.app_path') }}/{{ isset($category['slugs']['slug']) ? $category['slugs']['slug'] : '' }}">{{ $category['title'] }}</a></li>
                    @endforeach
                @endif

            </ul>

            <div class="buttons">

                <a href="{{config('app.app_path')}}/category" class="relCatBtn">{{ trans('sentence.category_store_view_all') }} <span>{{ trans('sentence.category_page_title') }}</span></a>

            </div>

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.category_detail_popular_brands') }} Popular Brands</h4>

            <ul class="sideBarCategories">

                @if(!empty($popularStores))
                    @foreach($popularStores as $store)
                        <li><a href="{{config('app.app_path')}}/{{ isset($store['slugs']['slug']) ? $store['slugs']['slug'] : '' }}">{{ $store['name'] }}</a></li>
                    @endforeach
                @endif
            </ul>

        </div>

        <div class="sidePannel cpnAlert subscribeNeverMisNewLetter">

            <h4>{{ trans('sentence.category_detail_coupon_alerts') }}</h4>

            <p>{{ trans('sentence.category_detail_never_miss') }}</p>
            <form id="subscribeNeverMisId">
                <input type="email" required="required" id="subscribeNeverMis" placeholder="email@address.com">
            </form>
        </div>

    </div>

</div>

@endsection