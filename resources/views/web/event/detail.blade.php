@extends('web.layouts.app')
@section('content')

<div class="flexWrapper strCpnDeal">

    <div class="contntWrpr">

        <div class="cupnsTextAlgment">

            <h3 class="pageTitle">{{$detail['title']}}</h3>

            <div class="rowbar">

                <div class="flexWrap cupnsAdjust">

                    @foreach($detail['coupons'] as $coupon)

                        @if($coupon['code']!='')

                        <div class="only-codes productBox fullPrd">

                            <div class="coupons">

                                <figure>

                                    <a href="javascript:;" class="logoAnchor">

                                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{$coupon['image']['url']}}" alt="" class="couponImage">

                                    </a>

                                </figure>

                                <div class="cpnDtlSec">

                                    <div class="textWrpr">

                                        <div class="title">

                                            <h4 class="offerTitle">{{$coupon['title']}}</h4>

                                            <span>{{ trans('sentence.page_exclusions_apply') }}</span>

                                        </div>

                                        <div class="expiry">

                                            <div class="views">

                                                <i class="lm_user"></i><span>{{ $coupon['viewed'] }} {{ trans('sentence.page_views') }}</span>

                                            </div>

                                            @if($coupon['verified']==1)
                                            <div class="verify">
                                                <i class="lm_check_square"></i><span>{{ trans('sentence.page_verified') }}</span>
                                            </div>
                                            @endif

                                            <div class="date">
                                                @php
                                                $expiryDate = date('d-M-yy', strtotime($coupon['date_expiry']));
                                                @endphp
                                                <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ $expiryDate }}</span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="buttonsWrapper">
                                        <div class="codeButton openOverlay" data-name="copycode">
                                            <a class="baseurlappend visibleButton" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}" data-marchant="{{ $coupon['affiliate_url'] }}" data-var="copy">
                                            <span class="buttonSpan"> {{ trans('sentence.page_get_codes') }} </span>
                                            </a>
                                            <span class="hiddenCode"> <?php echo uniqid(); ?> <span class="foldedCorner"></span> </span>
                                        </div>
                                    </div>

                                    

                                </div>

                                <i class="lm_right vsbanchr"></i>
                            <a class="cids responsiveLink" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}"></a>

                            </div>

                        </div>
                        @else
                        <div class="only-codes productBox fullPrd">

                            <div class="coupons">

                                <figure>

                                    <a href="javascript:;" class="logoAnchor">

                                    <img src="{{ config('app.image_path') }}/build/images/placeholder.png" data-src="{{$coupon['image']['url']}}" alt="" class="couponImage">

                                    </a>

                                </figure>

                                <div class="cpnDtlSec">

                                    <div class="textWrpr">

                                        <div class="title">

                                            <h4 class="offerTitle">{{$coupon['title']}}</h4>

                                            <!-- <p class="offerDesr"> +2.0% Cash Back</p> -->

                                            <span>{{ trans('sentence.page_exclusions_apply') }}</span>

                                        </div>

                                        <div class="expiry">

                                            <div class="views">

                                                <i class="lm_user"></i><span>{{ $coupon['viewed'] }} {{ trans('sentence.page_views') }}</span>

                                            </div>

                                            @if($coupon['verified']==1)
                                            <div class="verify">

                                                <i class="lm_check_square"></i><span>{{ trans('sentence.page_verified') }}</span>

                                            </div>
                                            @endif

                                            <div class="date">
                                                @php
                                                $expiryDate = date('d-M-yy', strtotime($coupon['date_expiry']));
                                                @endphp
                                                <i class="lm_clock"></i><span>{{ trans('sentence.home_expiry_date') }} {{ $expiryDate }}</span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="buttonsWrapper">

                                        <div class="codeButton openOverlay" data-name="copycode">

                                            <a class="baseurlappend DealButton" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}" data-marchant="{{ $coupon['affiliate_url'] }}" data-var="deal">
                                            <span class="buttonSpan"> {{ trans('sentence.page_get_deals') }} </span>
                                            </a>

                                        </div>

                                    </div>

                                </div>

                                <i class="lm_right vsbanchr"></i>
                                <a class="cids responsiveLink" data-id="{{ $coupon['id'] }}" data-store="{{ $coupon['affiliate_url'] }}"></a>

                            </div>

                        </div>
                        @endif
                    @endforeach
                    

                </div>

            </div>

        </div>

    </div>

    <div class="sidCntnt">

        <div class="sidePannel abBrCnt">

            <h2>{{ trans('sentence.page_event_detail_about') }} {{$detail['title']}}</h2>

            {!!html_entity_decode($detail['short_description'])!!}

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.page_related_categories') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($featuredCategories))
                    @foreach($featuredCategories as $category)
                        <li><a href="{{ config('app.app_path') }}/{{ $category['slugs']['slug'] }}">{{ $category['title'] }}</a></li>
                    @endforeach
                @endif

            </ul>

            <div class="buttons">

                <a href="{{config('app.app_path')}}/category" class="relCatBtn">{{ trans('sentence.page_view_all') }} <span>{{ trans('sentence.page_view_all_categories') }}</span></a>

            </div>

        </div>

        <div class="sidePannel SdePnlCat">

            <h4>{{ trans('sentence.page_brands_heading') }}</h4>

            <ul class="sideBarCategories">

                @if(!empty($popularStores))
                    @foreach($popularStores as $store)
                        <li><a href="{{ $store['slugs']['slug'] }}">{{ $store['name'] }}</a></li>
                    @endforeach
                @endif
            </ul>

        </div>

        <div class="sidePannel cpnAlert subscribeNeverMisNewLetter">

            <h4>{{ trans('sentence.category_detail_coupon_alerts') }}</h4>

            <p>{{ trans('sentence.category_detail_never_miss') }} </p>
            <form id="subscribeNeverMisId">
                <input type="email" required="required" id="subscribeNeverMis" placeholder="{{ trans('sentence.category_detail_email_place_holder') }}">
            </form>
        </div>

    </div>

</div>

@endsection