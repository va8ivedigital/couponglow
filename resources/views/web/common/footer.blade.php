<footer class="footer">

    <div class="quickLinks">

        <div class="flexWrapper">

            <div class="footerLinks">

                <h5>{{ trans('sentence.home_speciality_pages_heading') }}</h5>

                @if(!empty($bottom_event))
                    <ul>
                        @foreach($bottom_event as $event)
                            <li><a href="{{ config('app.app_path') }}/{{ $event['slugs']['slug'] }}">{{ $event['title'] }}</a></li>

                        @endforeach
                    </ul>
                @endif

            </div>

            <div class="footerLinks">

                <h5>{{ trans('sentence.home_connect_page_heading') }}</h5>

                <ul>

                    <li><a href="{{ $site_wide_data['facebook'] ?? ''}}" target="_blank">{{ trans('sentence.home_facebook_heading') }} </a></li>

                    <li><a href="{{ $site_wide_data['twitter'] ?? ''}}" target="_blank">{{ trans('sentence.home_twitter_heading') }}</a></li>

                    <li><a href="{{ $site_wide_data['linked_in'] ?? ''}}" target="_blank">{{ trans('sentence.home_linkedin_heading') }}</a></li>

                </ul>

                <div class="social">

                    <a href="{{ $site_wide_data['facebook'] ?? ''}}" target="_blank"><i class="lm_facebook"></i></a>

                    <a href="{{ $site_wide_data['twitter'] ?? ''}}" target="_blank"><i class="lm_twitter"></i></a>

                    <a href="{{ $site_wide_data['linked_in'] ?? ''}}" target="_blank"><i class="lm_linkedin"></i></a>

                    <a href="{{ $site_wide_data['youtube'] ?? ''}}" target="_blank"><i class="lm_youtube"></i></a>

                </div>

            </div>

            <div class="footerLinks resFooterMenu">

                <h5>{{ trans('sentence.home_general_heading') }}</h5>

                <ul>
                    @if (!empty($pages))
                        @foreach($pages as $page)
                            @if($page['bottom'] == 1)
                                <li><a href="{{ config('app.app_path') }}/{{ $page['slugs']['slug'] }}">{{ $page['title'] }}</a></li>
                            @endif
                        @endforeach
                    @endif

                    <li><a href="{{ config('app.app_path') }}/contact_us">{{ trans('sentence.home_contact_page_link') }}</a></li>

                    <li><a href="{{ config('app.app_path') }}/sitemap">{{ trans('sentence.home_site_map') }}</a></li>

                </ul>

            </div>

            <div class="newsLetter">

                <h4>{{ trans('sentence.home_what_we_are_about') }}</h4>

                <p>{{ trans('sentence.home_what_we_are_about_content') }}</p>

                <div class="subscribeBox">

                    <h5>{{ trans('sentence.home_news_letter_heading') }}</h5>

                    <p>{{ trans('sentence.home_news_letter_description') }}</p>

                    <form id="subBox">

                        <input type="email" id="subBoxEmail" name="subBoxEmail" placeholder="{{ trans('sentence.home_enter_your_Valid_email') }}" required="required">

                        <br>

                        <button type="submit">{{ trans('sentence.home_subscribe') }}</button>

                    </form>

                    <p class="successful">Congratulations! You’ll be the first to receive our latest Vouchers & Deals.</p>

                </div>

            </div>

        </div>

    </div>

</footer>

<div class="copyright">

    <div class="flexWrapper">

        <p>{{ trans('sentence.home_footer_all_rights_reserved') }}</p>

    </div>

</div>



<?php if(isset($_GET['copy'])){?>
<div id="copycode" class="overlayWrapper" style="display: flex;">
    <div class="popupWrpr">
        <div class="overlayBgReset"></div>

        <div class="overlayContainer">
            <div class="head">
                <a href="javascript:;" class="logo">
                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{ isset($couponRecord[0]['media'][0]['url']) ? $couponRecord[0]['media'][0]['url'] : config('app.image_path') . '/build/images/placeholder.png' }}" alt="">
                </a>
                <span class="closeOverlay">
                <i class="lm_close"></i>
                  </span>
            </div>
            @php
                $couponRecord = getCouponRecord($_GET['copy']);
            @endphp
            <div class="logoBox">
                <div class="logoImage">
                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{ isset($couponRecord[0]['media'][0]['url']) ? $couponRecord[0]['media'][0]['url'] : config('app.image_path') . '/build/images/placeholder.png' }}" alt="">
                </div>
                <div class="desc">
                    <div class="date">
                        <i class="lm_clock"></i><span>Exp: {{ date('d-M-yy', strtotime($couponRecord[0]['date_expiry'])) }}</span>
                    </div>
                    <p>{{ $couponRecord[0]['title'] }}</p>
                </div>
            </div>
            <div class="codeCopySec">
                <div class="inputWrp">
                    <input id="input_output" class="code" type="text" value="{{ $couponRecord[0]['code'] }}" readonly="">
                </div>
                <a id="copyCodeBtn" href="javascript:;" class="btn copyBtn copyCodeButton">COPY</a>
                <p class="copyFooter">Copy the code, paste it into the checkout box, and you will be suprised.</p>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if(isset($_GET['deal'])){?>

<div id="copycode" class="overlayWrapper" style="display: flex;">

    <div class="popupWrpr">

        <div class="overlayBgReset"></div>

        <div class="overlayContainer">

            <div class="head">

                <a href="javascript:;" class="logo">

                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{ isset($couponRecord[0]['media'][0]['url']) ? $couponRecord[0]['media'][0]['url'] : config('app.image_path') . '/build/images/placeholder.png' }}" alt="">

                </a>

                <span class="closeOverlay">

                <i class="lm_close"></i>

                  </span>

            </div>
            @php
                $couponRecord = getCouponRecord($_GET['deal']);
            @endphp
            <div class="logoBox">

                <div class="logoImage">

                    <img src="{{config('app.image_path')}}/build/images/placeholder.png" data-src="{{ isset($couponRecord[0]['media'][0]['url']) ? $couponRecord[0]['media'][0]['url'] : config('app.image_path') . '/build/images/placeholder.png' }}" alt="">

                </div>

                <div class="desc">

                    <div class="date">
                        <i class="lm_clock"></i><span>Exp: {{ date('d-M-yy', strtotime($couponRecord[0]['date_expiry'])) }}</span>

                    </div>

                    <p>Great promo 10% discount code for all products so hurry up!</p>

                </div>

            </div>

            <div class="codeCopySec">

                <a href="javascript:;" class="btn gotoBtn">Go To Store</a>

                <p>Deal Activated. No coupon required!</p>

            </div>

        </div>

    </div>

</div>

<?php } ?>


</body>
</html>



<?php
$ob_get_clean_css = ob_get_clean();

$cssmain  = preg_replace(array('/ {2,}/','/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'),array(' ',''),$ob_get_clean_css);

echo $cssmain;

?>
<script  src="{{ asset('build/js/all.js')}}"></script>
<link rel="stylesheet" href="{{ asset('build/css/jquery-ui.min.css')}}">
<script src="{{ asset('build/js/jquery-ui.min.js')}}"></script>

{!! isset($site_wide_data['javascript_tags']) ? $site_wide_data['javascript_tags'] : '' !!}

<script>
var baseTitle = window.document.title;
window.onblur = function () { document.title = 'Back to {{$site_wide_data['name'] ? $site_wide_data['name'] : ''}}'; }
window.onfocus = function () {   document.title = baseTitle; };
    $(document).on('submit', '#subBox', function() {
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: '{{ route('submitsubscribe') }}',
            type: 'POST',
            data: {
                "_token" : "{{ csrf_token() }}",
                "dataType" : "JSON",
                'data' : {
                    'email' : $('#subBoxEmail').val()
                },
            },
            success : function(data) {
                $('.subscribeBox').html(data.msg);

            },
            error : function(data) {
                console.log(data);

            }
        });
    });

    $("#subscribeNeverMisId").keypress(function(e) {
        if(e.which == 13) {
            event.preventDefault();
            var form = $(this);
            $.ajax({
                url: '{{ route('submitsubscribe') }}',
                type: 'POST',
                data: {
                    "_token" : "{{ csrf_token() }}",
                    "dataType" : "JSON",
                    'data' : {
                        'email' : $("#subscribeNeverMis").val()
                    },
                },
                success : function(data) {
                    $('.subscribeNeverMisNewLetter').html(data.msg);

                },
                error : function(data) {
                    console.log(data);

                }
            });

        }
    });


    $(document).on('submit', '#contactBox', function() {
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: '{{ route('contact.store') }}',
            type: 'POST',
            data: {
                "_token" : "{{ csrf_token() }}",
                "dataType" : "JSON",
                'data' : {
                    'name' : $('#name').val(),
                    'email' : $('#email').val(),
                    'contact' : $('#contact').val(),
                    'subject' : $('#subject').val(),
                    'message' : $('#message').val()
                },
            },
            success : function(data) {
                $('.outerContactBox').html(data.msg);
            },
            error : function(data) {
                console.log(data);
            }
        });
    });

    $(document).on('click', '.sortalpha', function(e){

        var target  = $(this).attr('data-target2');

        window.location.href= '{{ config("app.app_path") }}' +target;
    });
    $(document).on('click', '.baseurlappend', function(e){

        var varName = $(this).attr('data-id');
        var vartarg = $(this).attr('data-var');
        var varstor = $(this).data("store");
        var post_url = "{!! config('app.app_path').'/update-coupon-views' !!}";
        $_token = "{{ csrf_token() }}";
        $.ajax({
            url : post_url,
            type: 'GET',
            data: {"data_id" : varName}
        }).done(function(response){
            console.log(response);
        });
        window.open('{{ url()->current() }}' + "?"+vartarg+"=" + varName);
        location.replace(varstor);
    });


    $(".search_term_store").autocomplete({
        autoFocus: true,
        appendTo: $(".search_term_store").closest('.searchfield'),
        source: function (request, response) {
            var input = this.element;
            var search = request.term;
            $.ajax({
                url: '{{config("app.app_path")."/search_store" }}',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    search : search
                },
                success: function (data) {
                    if(data == 0) {
                        var result = [{
                            label: 'No matches found',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            return {
                                value: item['title'],
                                url:item['url']
                            }
                        }));
                    }
                }
            });
        },
        select: function (event, ui) {
            window.location.href = ui.item.url;
            self.element.val( "" );
        },
        minLength: 3
    });

    $(".search_blog").autocomplete({
        autoFocus: true,
        appendTo: $(".search_blog").closest('.d'),
        source: function (request, response) {
            var input = this.element;
            var search = request.term;
            $.ajax({
                url: '{{config("app.app_path")."/search_blog" }}',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    search : search
                },
                success: function (data) {
                    if(data == 0) {
                        var result = [{
                            label: 'No matches found',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            return {
                                value: item['title'],
                                url:item['url']
                            }
                        }));
                    }
                }
            });
        },
        select: function (event, ui) {
            window.location.href = ui.item.url;
            self.element.val( "" );
        },
        minLength: 3
    });

    $(document).on('click', '#load_more_button', function(e){

        var id = $(this).attr('data-id');
        var category_id = $(this).attr('blog-category-id');
        $('#load_more_button').html('<b>Loading...</b>');
        var post_url = "{!! config('app.app_path').'/load-more-data' !!}";
        $_token = "{{ csrf_token() }}";
        $.ajax({
            url : post_url,
            type: 'GET',
            data: {
                "_token": "{{ csrf_token() }}",
                "data_id" : id,
                "category_id" : category_id
            }
        }).done(function(response){
            $('#load_more_button').remove();
            $('#post_data').append(response);
            console.log(response);
        });

    });

    $(document).on('click', '#blog_load_more_button', function(e){

        var id = $(this).attr('data-id');
        var author_id = $(this).attr('blog-author-id');
        $('#blog_load_more_button').html('<b>Loading...</b>');
        var post_url = "{!! config('app.app_path').'/author-load-more-data' !!}";
        $_token = "{{ csrf_token() }}";
        $.ajax({
            url : post_url,
            type: 'GET',
            data: {
                "_token": "{{ csrf_token() }}",
                "data_id" : id,
                "author_id" : author_id
            }
        }).done(function(response){
            $('#blog_load_more_button').remove();
            $('#post_data').append(response);
            console.log(response);
        });

    });




    var page = 1;
    $(window).scroll(function() {

        @if(!isset($_GET['q']));

        if($(window).scrollTop() + $(window).height() >= $(document).height()) {

            page++;
            loadMoreData(page);

        }
        @endif

    });

    function loadMoreData(page){

        var id = $("#last_id").val();
        $('#blog_load_more_button').html('<b>Loading...</b>');
        var post_url = "{!! config('app.app_path').'/store-load-more-data' !!}";
        $_token = "{{ csrf_token() }}";

        $.ajax({
            url : post_url,
            type: 'GET',
            data: {
                "_token": "{{ csrf_token() }}",
                "last_id" : id
            }
        }).done(function(response){
            $('#blog_load_more_button').remove();
            $('.remove_last_id').remove();
            $('#post_data').append(response);
            console.log(response);
        });
    }

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('{{config('app.image_path')}}/sw.js')
            .then(function() { console.log("Service Worker Registered"); });


    }
</script>
