<?php ob_start(); ?>
<?php header('X-Robots-Tag: noindex, nofollow'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		@php
		$description = (isset($meta['description'])) ? $meta['description'] : 'Coupon Glow'  ;
		$title = (isset($meta['title'])) ? $meta['title'] : 'Coupon Glow'  ;
		$keywords = (isset($meta['keywords'])) ? $meta['keywords'] : 'Coupon Glow'  ;
		@endphp
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, initial-scale=1.0">
		<meta name="description" content="{{ $description }}" />
		<meta name="keywords" content="{{ $keywords }}">
        <link rel="icon" href="{{ isset($site_wide_data['favicon']['thumbnail']) ? $site_wide_data['favicon']['thumbnail'] : '' }}" type="image/x-icon">

        {!! isset($site_wide_data['html_tags']) ? $site_wide_data['html_tags'] : '' !!}

		<title>{{ $title }}</title>
		<style>
			<?php
			if(isset($pageCss)){
				$css = asset("build/css/$pageCss.css");
				readfile("build/css/$pageCss.css");
			}else{
				$css = asset("build/css/main.css");
				readfile("build/css/main.css");
			}
			?>
		</style>

	</head>
	<body>

	<header class="header">

				<div class="Top">

					<div class="flexWrapper">

						<span class="menu" onclick="openNav()">

							<i class="lm_menu"></i>

						</span>

						<div class="logo">

							<a href="{{config('app.app_path')}}" class="<?php echo (strpos($_SERVER['PHP_SELF'], 'index.php') ? ' active' : '');?>" title="">

								<img src="{{ isset($site_wide_data['logo']['url']) ? $site_wide_data['logo']['url'] : config('app.image_path') . '/build/images/couponglow_logo.png' }}" alt="">

							</a>

						</div>

						<span class="searchRes">

							<i class="lm_search"></i>

						</span>

						<div class="searchPanel">
							<div class="searchfield" style="overflow: visible;">
                                  <form id="store_search_form" style="height: 100%;">
                                    <input type="text" class="search_term_store" placeholder="{{ trans('sentence.home_search_placeholder') }}">
                                      <button type="submit"><i class="lm_search"></i></button>
                                </form>
							</div>
						</div>

					</div>

				</div>

				<nav class="links navigationBar sidenav">

					<div class="flexWrapper">

						<a href="{{ config('app.app_path') }}/sitemap" class="active">All Stores</a>

						<a href="{{config('app.app_path')}}/category">Categories</a>

						@if (!empty($top_event ))
							@foreach($top_event as $event)
								<a href="{{ config('app.app_path') }}/{{ $event['slugs']['slug'] }}">{{ $event['title'] }}</a>
							@endforeach
						@endif

						<a href="{{ config('app.app_path') }}/contact_us">Contact</a>

						<a href="{{ config('app.app_path') }}/blog" >Blog</a>


					</div>

				</nav>

			</header>

