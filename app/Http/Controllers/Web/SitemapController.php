<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Category;
use App\Page;
use App\SiteSetting;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
class SitemapController extends Controller {
  public function __construct() {
  }
  public function index() {
    $data = [];
    return view('web.sitemap.index')->with($data);
  }
}