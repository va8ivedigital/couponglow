<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Store;
use App\Page;
use App\SiteSetting;
use App\Blog;
use App\Category;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class StoresController extends Controller {
  public function __construct() {
      $this->store_model = new Store();
  }
  public function index(Request $request) {
    $data = [];
    try{
        $siteid = config('app.siteid');
        $data['featuredCategories'] = Category::with('slugs')->with('sites')->whereHas('sites', function($q2) use ($siteid) {
        $q2->where('site_id',$siteid);
        })->where('featured',1)->where('publish',1)->orderBy('title', 'asc')->get()->toArray();

        $data['popularStores'] = Store::select('id','name')->where('popular',1)->with('slugs')->where('publish',1)
                ->with('sites')->whereHas('sites', function($q) use ($siteid){
          $q->where('site_id', $siteid);
        } )->orderBy('name', 'asc')->get()->toArray();

        if($request->Input('q')){
		
            $data['list'] = $this->store_model->getAllStores($siteid,$request->Input('q'));
            $data['popular'] = $this->store_model->getPopularStores($siteid);
            return view('web.store.index')->with($data);
		}else{
    		$siteid = config('app.siteid');
            $data['list'] = $this->store_model->getAllStores($siteid);
            $data['popular'] = $this->store_model->getPopularStores($siteid);
            return view('web.store.index')->with($data);
		}
    }catch (\Exception $e) {
        abort(404);
    }
   
  }
    public function detail() {
        $data = [];
        try{
            $siteid = config('app.siteid');
            $dt = Carbon::now();
            $date = $dt->toDateString();

             $data['detail'] = Store::with('storeCoupons')->with(['storeCoupons'=> function($s) use ($date,$siteid){
                 $s->where('date_expiry', '>=', $date)->CustomWhereBasedData($siteid);
             }])->with('categories')->with('sites')->whereHas('sites', function($q) use ($siteid) {
             $q->where('site_id',$siteid);
             } )->where('publish',1)->where('id',PAGE_ID)->first()->toArray();
            $data['featuredCategories'] = Category::select('id','title')->with('slugs')->with('sites')->whereHas('sites', function($q2) use ($siteid) {
                $q2->where('site_id',$siteid);
                })->where('featured',1)->where('publish',1)->orderBy('title', 'asc')->get()->toArray();
            $data['popularStores'] = Store::select('id','name')->where('popular',1)->with('slugs')->where('publish',1)
                        ->with('sites')->whereHas('sites', function($q) use ($siteid){
                  $q->where('site_id', $siteid);
                } )->orderBy('name', 'asc')->get()->toArray();
            $meta['title']=$data['detail']['meta_title'];
            $meta['keywords']=$data['detail']['meta_keywords'];
            $meta['description']=$data['detail']['meta_description'];
            $data['meta']=$meta;

            return view('web.store.detail')->with($data);
        }catch (\Exception $e) {
                abort(404);
        }
   
    }
    
    public function search(Request $request){
        $data = [];
        try{
            $_search_keyword = $request->search;
            \DB::enableQueryLog();
            $siteid = config('app.siteid');
            $stores = Store::where('name','LIKE','%'.$_search_keyword.'%')->with('sites')->whereHas('sites', function ($site) use ($siteid){
                    $site->where('site_id', $siteid);
            })->where('publish',1)->get();

        if ($stores) {
			foreach ($stores as $k => $v) {
				$data[$k]['title'] = $v->name;
				$data[$k]['url'] = config('app.app_path').'/'.$v->slugs->slug;
			}
			return ($data);
		} else {
			return 0;
		}
        }catch (\Exception $e) {
                abort(404);
        }
    }

    public function searchBlog(Request $request){
        $data = [];
        try{
            $_search_keyword = $request->search;
            \DB::enableQueryLog();
            $siteid = config('app.siteid');
            $blogs = Blog::where('title','LIKE','%'.$_search_keyword.'%')->with('slugs')->with('sites')->whereHas('sites', function($site) use ($siteid){
                $site->where('site_id', $siteid);
            })->get();

        if ($blogs) {
            foreach ($blogs as $k => $v) {
                $data[$k]['title'] = $v->title;
                $data[$k]['url'] = config('app.app_path').'/'.$v['slugs']['slug'];
            }
            return ($data);
        } else {
            return 0;
        }
        }catch (\Exception $e) {
                abort(404);
        }
    }

    public function storeLoadMoreData(Request $request){
        $siteid = config('app.siteid');
        if($request->ajax()){
            if($request['last_id'] > 0){
                $output = '';
                $last_id = '';
                $list = Store::where('id', '<', $request['last_id'])->where('publish',1)
                ->with('sites')->whereHas('sites', function($q2) use ($siteid) {
                $q2->where('site_id',$siteid);
                })->orderBy('id', 'desc')->limit(20)->get()->toArray();

                if(!empty($list)){
                    foreach($list as $store){
                        $url = config("app.app_path")."/".$store['slug'] ;
                        $image = $store['image']['url'];

                        $output .= '
                          <div class="stores">

                            <a href="'.$url.'" class="image">

                            <span>

                            <img src="'.$image.'" data-src="'.$image.'" alt="">

                            </span>

                            </a>

                        </div>
                          ';
                        $last_id = $store['id'];
                    }

                    $output .= '
                         <div class="remove_last_id"><input type="hidden" id="last_id" value="'.$last_id.'"></div>
                       ';

                    echo $output;

                }
            }
        }
    }
}