<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Coupon;
use App\Site;
use App\Event;
use App\Category;
use App\Store;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class EventsController extends Controller {
  public function detail(){
    $data = [];
    try{
      $siteid = config('app.siteid');
      $dt = Carbon::now();
      $date = $dt->toDateString();

      // $data['detail'] = Event::with('categories')->with('slugs')->with('stores')->with(['coupons'=> function($d) use ($date){
      //   $d->where('date_expiry', '>=', $date);
      // } ])->with(['sites'=> function($q) use ($siteid) {
      // $q->where('site_id',$siteid);
      // } ])->where('publish',1)->where('id',PAGE_ID)->first()->toArray();

      $data['detail'] = Event::select('id','title','short_description','long_description','meta_title','meta_keywords','meta_description')->with('categories')->with('stores')->with(['coupons'=> function($d) use ($date){
        $d->select(['id','title','description','affiliate_url','verified','sort','date_expiry','code','viewed'])->where('date_expiry', '>=', $date)->wherePublish(1);
      } ])->with('coupons.store:id')->CustomWhereBasedData($siteid)->where('id',PAGE_ID)->first()->toArray();

      if($data['detail'] == null){
          abort(404);
      }
      
      $meta['title']=$data['detail']['meta_title'];
      $meta['keywords']=$data['detail']['meta_keywords'];
      $meta['description']=$data['detail']['meta_description'];
      $data['meta']=$meta;

      $data['featuredCategories'] = Category::select('id','title')->with(['slugs' => function ($slugQuery){
        $slugQuery->select('id','obj_id','slug','old_slug');
      }])->whereHas('sites', function($q2) use ($siteid) {
                $q2->where('site_id',$siteid);
                })->where('featured',1)->where('publish',1)->orderBy('title', 'asc')->get()->toArray();

      $data['popularStores'] = Store::select('id','name')->where('popular',1)->with(['slugs' => function($slugQuery){
        $slugQuery->select('id','obj_id','slug','old_slug');
      }])->where('publish',1)->whereHas('sites', function($q) use ($siteid){
            $q->where('site_id', $siteid);
          } )->orderBy('name', 'asc')->get()->toArray();
      
      return view('web.event.detail')->with($data);
    }catch (\Exception $e) {
      abort(404);
    }
  }

}