<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Subscriber;
use App\Contact;
use App\Category;
use App\Store;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    public function submitSubscribe( Request $request ) {
      $data = $request->all();
      $model = new Subscriber;
      $checkSubscriber = $model->where('email', $data['data']['email'])->first();
      if($checkSubscriber){
      	return response()->json([
  	        'success' => true,
  	        'msg' => '<div class="finalCartPopup" style="display : block;">
  	        <div class="msg errorMsg finalCartPopup_errorMsg" style="display : block; padding-bottom: 18px;font-size: 14px;line-height: 20px;">
  	        You are already subscriber! We will contact you soon.
  	        </div>
  	        <div>
  	        <button type="button" onclick="jQuery(\'.finalCartPopup\').hide(\'slow\'); "class="button cn_shp btn"><span>Close</span></button>
  	        </div>
  	        </div>'
  	      ]);
        }
        $details = ip_details(get_client_ip());
        if($details['ip'] == '::1'){
          $model->email = $data['data']['email'];
          $model->page_link = url()->current();
          $model->longitude = '12.4343434343';
          $model->latitude = '98.8988776655';
          $model->country = "Pakistan";
          $model->region = "Sindh";
          $model->city = "Karachi";
          $model->ip = $details['ip'];
          $model->client_agent = $details['ip'];
          $model->site_id = config('app.siteid');
          $model->save();
        return response()->json([
          'success' => true,
          'msg' => '<div class="finalCartPopup" style="display : block;">
          <div class="msg errorMsg finalCartPopup_errorMsg" style="display : block; padding-bottom: 18px;font-size: 14px;line-height: 20px;">
          Thanks for contacting us! We will get back to you shortly.
          </div>
          <div>
          <button type="button" onclick="jQuery(\'.finalCartPopup\').hide(\'slow\'); "class="button cn_shp btn"><span>Close</span></button>
          </div>
          </div>'
        ]);
        }else{
          $model->email = $data['data']['email'];
          $model->page_link = url()->current();
          $loc = explode(',', $details['loc']);
          $model->longitude = $loc[0];
          $model->latitude = $loc[1];
          $model->country = $details['country'];
          $model->region = $details['region'];
          $model->city = $details['city'];
          $model->ip = get_client_ip();
          $model->client_agent = $request->server('HTTP_USER_AGENT');
          $model->site_id = config('app.siteid');
          $model->save();
        return response()->json([
          'success' => true,
          'msg' => '<div class="finalCartPopup" style="display : block;">
          <div class="msg errorMsg finalCartPopup_errorMsg" style="display : block;">
          Thanks for contacting us! We will get back to you shortly.
          </div>
          <div>
          <button type="button" onclick="jQuery(\'.finalCartPopup\').hide(\'slow\'); "class="button cn_shp"><span>Close</span></button>
          </div>
          </div>'
        ]);
        }


  }
  public function contactDetails(){
    $data = [];
    $siteid = config('app.siteid');

    $featuredCategories = Category::select('id','title')->with(['slugs' => function($slugQuery){
      $slugQuery->select('id','obj_id','slug','old_slug');
    }])->whereHas('sites', function($q2) use ($siteid) {
        $q2->where('site_id',$siteid);
        })->where('featured',1)->where('publish',1)->orderBy('title', 'asc')->get()->toArray();

    $popularStores = Store::select('id','name')->where('popular',1)->with(['slugs' => function ($slugQuery){
      $slugQuery->select('id','obj_id','slug','old_slug');
    }])->where('publish',1)->whereHas('sites', function($q) use ($siteid){
          $q->where('site_id', $siteid);
        } )->orderBy('name', 'asc')->get()->toArray();

    // dd($featuredCategories, $popularStores);

    return view('web.contact.index', compact('featuredCategories', 'popularStores'));
  }
  public function contactStore(Request $request){
    $data = $request->except(['_token']);
    $model = new Contact;
    $checkContactUser = $model->where('email', $data['data']['email'])->first();
    if($checkContactUser){
      return response()->json([
            'success' => true,
            'msg' => '<div class="finalCartPopup" style="display : block;">
            <div class="msg errorMsg finalCartPopup_errorMsg" style="display : block;">
            You are already contact memeber! We will contact you soon.
            </div>
            <div>
            <button type="button" onclick="jQuery(\'.finalCartPopup\').hide(\'slow\'); "class="button cn_shp"><span>Close</span></button>
            </div>
            </div>'
      ]);
    }
    $model->name = $data['data']['name'];
    $model->email = $data['data']['email'];
    $model->subject = $data['data']['subject'];
    $model->message = $data['data']['message'];
    $model->save();
    return response()->json([
        'success' => true,
        'msg' => '<div class="finalCartPopup" style="display : block;">
        <div class="msg errorMsg finalCartPopup_errorMsg" style="display : block;">
        Thanks for contacting us! We will get back to you shortly.
        </div>
        <div>
        <button type="button" onclick="jQuery(\'.finalCartPopup\').hide(\'slow\'); "class="button cn_shp"><span>Close</span></button>
        </div>
        </div>'
      ]);
  }
}
