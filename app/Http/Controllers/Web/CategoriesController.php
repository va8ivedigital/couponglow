<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Category;
use App\Page;
use App\SiteSetting;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
class CategoriesController extends Controller {
  public function __construct() {
  }
  public function index() {

      $data = [];
      try{
          $siteid = config('app.siteid');
          $data['list'] = Category::select('id','title')->CustomWhereBasedData($siteid)->orderBy('title')->get()->toArray();
          // dd($data);
          return view('web.category.index')->with($data);
      }catch (\Exception $e) {
          abort(404);
      }
  }
    public function detail() {
        $data = [];
        try{
            $siteid = config('app.siteid');
            $data['detail'] = Category::select('id','title')->with(['categoryStores'=>function($q1) use ($siteid){
                $q1->CustomWhereBasedData($siteid);
            }])->CustomWhereBasedData($siteid)->with('parentCategories')->where('id',PAGE_ID)->first();

            if($data['detail'] == null){
              abort(404);
            }

            $data['popular'] = Category::select('id','title')->CustomWhereBasedData($siteid)->orderBy('title')->get()->toArray();


            $meta['title']=$data['detail']['meta_title'];
            $meta['keywords']=$data['detail']['meta_keywords'];
            $meta['description']=$data['detail']['meta_description'];
            $data['meta']=$meta;
            return view('web.category.detail')->with($data);
        }catch (\Exception $e) {
            //dd($e);
            abort(404);
        }
    }
}
