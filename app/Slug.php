<?php
namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
class Slug extends Model
{
    protected $table = 'slugs';
    protected $primaryKey = 'id';
    protected $forignKey = 'obj_id';
    protected $table_field_name = 'table_name';
    protected $slug_field_name = 'slug';

    protected $fillable = [];
    protected $guarded  = [];

    public function blog() {
        return $this->belongsTo('App\Blog', 'obj_id', 'blog_id');
    }

    public function deleteSlug($obj_id, $table_name){
        $this->where($this->forignKey, $obj_id)->where($this->table_field_name, $table_name)->delete();
        return true;
    }

    public function insertSlug($last_id, $params, $table_name, $site_id, $parent_id = 0){
        $return_data = array();

        foreach ($site_id as $site) {
            if(empty($last_id) || empty($params) || empty($table_name)) return;

            $check_exist = $this->where($this->table_field_name, $table_name)->where($this->slug_field_name, $params)->where('site_id', $site)->count();
            if($check_exist > 0){
                $return_data['errors'] = ['error' => ['Slug is not unique']];
                $return_data['status'] = false;
                return $return_data;
            }

            $slug_ins = new $this;
            $slug_ins->obj_id = $last_id;
            $slug_ins->table_name = $table_name;
            $slug_ins->page_type = $table_name;
            $slug_ins->slug = $params;
            $slug_ins->site_id = $site;
            $slug_ins->slug_parent_id = $parent_id;
            $slug_ins->created_at = date('Y-m-d H:i:s');
            $slug_ins->updated_at = date('Y-m-d H:i:s');
            $slug_ins->save();
        }

        $return_data['status'] = true;
        return $return_data;
    }


    public function updateSlug($last_id, $params, $table_name, $site_id, $parent_id = 0, $old_slug = 0){
        $return_data = [];
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }

        if(empty($last_id) || empty($params) || empty($table_name)) return;
            // checking for slug if found on same obj id
            // then ok
            // else if not found on same obj id but
            // found on some different id
            // then it means this slug is not unique
            $checking = $this->where('obj_id', $last_id)->where('table_name', $table_name)
                /*->where('slug', $params)*/
            ->first();
        if(count($checking) > 0 && count($checking) < 2) {
            $checking->slug_parent_id = $parent_id;
            $checking->slug = $params;
            $checking->old_slug = $old_slug;
            $checking->updated_at = date('Y-m-d H:i:s');
            $checking->save();
            $return_data['status'] = true;
            return $return_data;
        }
        else if(count($checking) > 0 && count($checking) >= 2) {
            $return_data['errors'] = ['error' => ['Slug is not unique']];
            $return_data['status'] = false;
            return $return_data;
            $checking = $this->where('table_name', $table_name)->where('slug', $params)->first();
        }
        else {
            // if previously this has not assigned any slug so create one for this
            $slug_ins = new $this;
            $slug_ins->obj_id = $last_id;
            $slug_ins->table_name = $table_name;
            $slug_ins->page_type = $table_name;
            $slug_ins->slug = $params;
            $slug_ins->slug_parent_id = $parent_id;
            $slug_ins->created_at = date('Y-m-d H:i:s');
            $slug_ins->updated_at = date('Y-m-d H:i:s');
            $slug_ins->save();
            $return_data['status'] = true;
            return $return_data;
        }
    }


    public function add_new_slug($data)
    {
        try {
            DB::beginTransaction();
            $this->insert($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->catch_exception($e);
            // something went wrong
        }
        return false;
    }
    public function get_slug($link,$siteid){
        $data = $this->where(function($q) use ($link){
            $q->Orwhere('slug', $link)->Orwhere('old_slug', $link);
        })->where('site_id',$siteid)->first();
        return $data;
    }
}
