<?php
Route::get('/us', function () {
    return redirect('/');
});
Route::group(['namespace' => 'Web', 'prefix' => config('app.route_prefix')], function () {
//Route::group(['as' => 'web.', 'namespace' => 'Web'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/category', 'CategoriesController@index');
    Route::get('/sitemap', 'StoresController@index');
    Route::get('/blog', 'BlogsController@index')->name('blog');
    Route::get('/load-more-data', 'BlogsController@load_data');
    Route::get('/blog/author/{slug}', 'BlogsController@blogAuthor');
    Route::get('/author-load-more-data', 'BlogsController@authorLoadMoreData');
    Route::get('/store-load-more-data', 'StoresController@storeLoadMoreData');
    Route::get('/product', 'Product_categoriesController@index')->name('product');
    Route::get('/coupons', 'CouponController@index')->name('coupons');
    Route::get('/update-coupon-views', 'CouponController@updateCouponViews');
    Route::post('_subscribe', 'ContactController@submitSubscribe')->name('submitsubscribe');
    Route::get('/contact_us', 'ContactController@contactDetails');
    Route::post('contactStore', 'ContactController@contactStore')->name('contact.store');
    Route::get('/search_store', 'StoresController@search');
    Route::get('/search_blog', 'StoresController@searchBlog');
 
  //Route::get('{slug?}', 'SlugController@slugFinder')->name('slugFinder');
  if(defined('SLUG_LINK')){
  if(ROUTE_NAME == 'pages'){
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'stores'){
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'events') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'blogs') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'presses') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'categories') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
     // dd(ROUTE_NAME,SLUG_CONTROLLER,SLUG_LINK);
  }
  if(ROUTE_NAME == 'pages') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  }
  if(ROUTE_NAME == 'product_categories') {
    Route::get(SLUG_LINK, SLUG_CONTROLLER.'@detail')->name(ROUTE_NAME);
  } 
}else{
    Route::match(["get","post"], '/{parm1?}/{parm2?}/{parm3?}/{parm4?}', 'HomeController@_404')->name('FourOFour');
}
});